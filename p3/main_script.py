from sklearn.datasets import load_digits
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.svm import SVC



"""
Margen suave SVM

a) Formulacion margen suave SVM lineal

clasificacion binaria: 0 y otros digitos

||w|| en funcion de  

c in {10^-5,10^-3,10^-1,1,10,100} 

split = 0.75



"""

sns.set()

X , y = load_digits ( return_X_y = True )

split = .75


C = [10^-5,10^-3,10^-1,1,10,100]

# Graficar w en relacion a W

 #problema a)

clasificacion = np.array([1 if v==0 else -1 for v in y])

X_train, X_test, y_train, y_test = train_test_split(
    X,
    clasificacion,
    test_size=split)

print(X_train.shape, y_train)

# Create a classifier: a support vector classifier
clf = SVC(gamma=0.001)

# Learn the digits on the train subset
clf.fit(X_train, y_train)

# Predict the value of the digit on the test subset
predicted = clf.predict(X_test)


_, axes = plt.subplots(nrows=1, ncols=4, figsize=(10, 3))
for ax, image, prediction in zip(axes, X_test, predicted):
    ax.set_axis_off()
    image = image.reshape(8, 8)
    ax.imshow(image, cmap=plt.cm.gray_r, interpolation="nearest")
    ax.set_title(f"Prediction: {prediction}")

plt.show()
