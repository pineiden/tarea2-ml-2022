from pathlib import Path
import pandas as pd
from rich import print
from sklearn.preprocessing import PolynomialFeatures
import numpy as np

def generate_poly_x(X):
        poly2 = PolynomialFeatures(2)
        poly3 = PolynomialFeatures(3)
        Xpol2 = np.delete(
            poly2.fit_transform(X),
            0,
            axis=1)
        Xpol3 = np.delete(
            poly3.fit_transform(X),
            0,
            axis=1)
        return Xpol2, Xpol3

def read_data(datapath:Path):
    if datapath.exists():
        dataset = pd.read_csv(datapath)
        X = dataset[["deita","saiens"]].to_numpy()
        y = dataset["target"].to_numpy()
        Xpol2,Xpol3 = generate_poly_x(X)
        print(dataset.head())
        print(Xpol2)
        print(Xpol3)
    else:
        print(f"Archivo de datos {datapath} no existe")


if __name__ == '__main__':
    main = Path(__file__).parent.parent
    datapath = main / "documentos/abstract_data.csv"
    read_data(datapath)
