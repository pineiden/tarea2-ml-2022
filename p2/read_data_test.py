from datetime import datetime
from pathlib import Path
from pydantic.dataclasses import dataclass
from dataclasses import asdict
from rich import print
import pandas as pd
# IMPORTAR DESDE EL ARCHIVO QUE CORRESPONDA
from logistic_regression import LogisticRegression, MatrixNxD, MatrixNx1_C
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
import sys
import multiprocessing
from typing import Dict, List
from multiprocessing import Pool, Manager
import asyncio
import concurrent.futures as cf
from functools import partial
from scoring import (
    Montecarlo, 
    TotalMontecarlo, 
    Scoring, 
    mc_parallel, 
    run_group, 
    run_queue_montecarlo)
import psutil 
import sys
from sklearn.preprocessing import PolynomialFeatures
import click
import seaborn as sns
import itertools

def generate_poly_x(X):
    # cambiar 2->1
    poly1 = PolynomialFeatures(1)
    poly3 = PolynomialFeatures(3)
    Xpol1 = np.delete(
        poly1.fit_transform(X),
        0,
        axis=1)
    Xpol3 = np.delete(
        poly3.fit_transform(X),
        0,
        axis=1)
    return Xpol1, Xpol3


def read_data(datapath:Path):
    if datapath.exists():
        dataset = pd.read_csv(datapath)
        X = dataset[["deita","saiens"]].to_numpy()
        y = dataset["target"].to_numpy()
        Xpol1,Xpol3 = generate_poly_x(X)
        print("Xshapes",Xpol1.shape, Xpol3.shape)
        return (X,Xpol1,Xpol3), y
    else:
        print(f"Archivo de datos {datapath} no existe")



def load_dataset(pathname:Path):
    try:
        databytes = pathname.read_bytes()
        dataset = pickle.loads(databytes)
        return dataset.grouped()
    except Exception as e:
        raise e

@click.command()
@click.option(
    "--data", 
    type=click.Path(exists=True), 
    help="ruta de datapath")
@click.option(
    "--model", 
    type=click.Path(exists=True), 
    help="modelpath")
def run(data, model):
    here = Path(__file__).parent 
    datapath = Path(data)
    modelpath = Path(model)
    if datapath.exists():
        (X, Xp1, Xp3), y = read_data(datapath)
        #read models and select
        print(X.shape)
    else:
        print("Path to dataset {datapath} does not exists")
    #run_parallel(datapath, iterations, stop_condition)
    if modelpath.exists():    
        models = load_dataset(modelpath)
        model_p1 = models["lineal"].pop().model
        model_p3 = models["poly3"].pop().model
        
        y_p1_pred = model_p1.predict(Xp1)
        y_p3_pred = model_p3.predict(Xp3)
        
        print("Shapes=>")
        print(y_p1_pred.shape, y_p3_pred.shape, y.shape)
        score_p1 = model_p1.score(y, y_p1_pred)
        score_p3 = model_p3.score(y, y_p3_pred)

        print("P2=>",score_p1, "P3=>",score_p3)
        print("Akaike P2=>",model_p1.aic, "P3=>",model_p3.aic)
        print("BIC P2=>",model_p1.bic, "P3=>",model_p3.bic)

    else:
        print("Path to model {modelpath} does nott exists")

if __name__ == "__main__":
    run()
