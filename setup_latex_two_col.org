#+LATEX_CLASS_OPTIONS: [legalpaper,9pt,twoside,twocolumn,margin=.5in]
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LATEX_HEADER:  \selectlanguage{spanish}
#+LATEX_HEADER: \usepackage{fontspec}
#+LATEX_HEADER: \usepackage[T1]{fontenc}
#+LATEX_HEADER: \usepackage[utf8]{inputenc}
#+LATEX_HEADER_EXTRA: \usepackage{geometry}
#+LATEX_HEADER_EXTRA: \usepackage{amsmath}
#+LATEX_HEADER_EXTRA: \usepackage{amssymb}
#+LATEX_HEADER_EXTRA: \usepackage{upgreek}

#+LATEX_HEADER_EXTRA: \geometry{margin=1.0cm, top=1.0cm, headsep=1.0cm, footskip=1.0cm}
#+LATEX_HEADER_EXTRA: \newcommand{\R}{\ensuremath{\mathbb{R}}}
#+STARTUP: latexpreview

#+begin_src emacs-lisp
(setq org-format-latex-options (plist-put org-format-latex-options :scale 2.0))
#+end_src

#+RESULTS:
| :foreground | default | :background | defalut | :scale | 2.0 | :html-foreground | Black | :html-background | Transparent | :html-scale | 1.0 | :matchers | (begin $1 $ $$ \( \[) |
